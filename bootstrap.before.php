<?php

/**
 * This file will make Vanilla use a different config depending on folder you're on.
 */
call_user_func(function () {
//    $host = $_SERVER['HTTP_HOST'];
//    [$host, $port] = explode(':', $host, 2) + ['', ''];
//
//    // Whitelist to a domain. This can probably get removed at some point.
//    if (in_array($host, ['dev.vanilla.localhost'], true)) {
//        // This is the default conf/config.php based install.
//        return;
//    } elseif (!in_array($host, ['vanilla.localhost'], true)) {
//        // This is a conf/{$host}.php based install.
//        $configPath = PATH_ROOT . "/conf/$host.php";
//    } else {
//        // This domain treats the root directory as its own virtual root.
//        [$root, $_] = explode('/', ltrim($_SERVER['SCRIPT_NAME'], '/'), 2) + ['', ''];
//        // Use a config specific to the site.
//        $configPath = PATH_ROOT . "/conf/$host/$root.php";
//        $_SERVER['NODE_SLUG'] = $root;
//    }
//
//    if (!file_exists(dirname($configPath))) {
//        mkdir(dirname($configPath), 0755, true);
//    }
//
//    define('PATH_CONF_DEFAULT', $configPath);


    // // Cache
    // saveToConfig('Cache.Enabled', true);
    // saveToConfig('Cache.Method', 'memcached');
    // saveToConfig('Cache.Memcached.Store', ['memcached:11211']);

    // if (c('Cache.Enabled')) {
    //     if (class_exists('Memcached')) {
    //         saveToConfig(
    //             'Cache.Memcached.Option.'.Memcached::OPT_COMPRESSION, true, false
    //         );
    //         saveToConfig(
    //             'Cache.Memcached.Option.'.Memcached::OPT_DISTRIBUTION,
    //             Memcached::DISTRIBUTION_CONSISTENT,
    //             false,
    //         );
    //         saveToConfig(
    //             'Cache.Memcached.Option.'.Memcached::OPT_LIBKETAMA_COMPATIBLE,
    //             true,
    //             false,
    //         );
    //         saveToConfig(
    //             'Cache.Memcached.Option.'.Memcached::OPT_NO_BLOCK, true, false
    //         );
    //         saveToConfig(
    //             'Cache.Memcached.Option.'.Memcached::OPT_TCP_NODELAY, true, false
    //         );
    //         saveToConfig(
    //             'Cache.Memcached.Option.'.Memcached::OPT_CONNECT_TIMEOUT,
    //             1000,
    //             false,
    //         );
    //         saveToConfig(
    //             'Cache.Memcached.Option.'.Memcached::OPT_SERVER_FAILURE_LIMIT,
    //             2,
    //             false,
    //         );
    //     } else {
    //         die('PHP is missing the Memcached extension.');
    //     }
    // }
});
